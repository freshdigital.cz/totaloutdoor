<?php
    $active="blog";
    include('header.php');
?>
<div class="row">
	<div class="container">
		<div class="row">
            <div class="col-md-12">
                <div class="hl-title">
                    <h2><span>Blog</span></h2>
                </div>
                <p class="blog-checkbox-description">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore
                    et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat.
                </p>
                <div class="checkbox-volba row">
                    <div class="col-md-3 col-sm-3 col-xs-12 checkbox-blok1">
                        <input type="checkbox" name="cestovani" value="cestovani"> <p class="blog-checkbox">Cestovaní</p>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-12 checkbox-blok">
                        <input type="checkbox" name="technologie" value="technologie"> <p class="blog-checkbox">Technológie</p>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-12 checkbox-blok">
                        <input type="checkbox" name="nove-produkty" value="nove-produkty"> <p class="blog-checkbox">Nové produkty</p>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-12 checkbox-blok">
                        <input type="checkbox" name="na-hory" value="na-hory"> <p class="blog-checkbox">Na hory</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="margin-top:5%">
            <div class="col-md-6 col-sm-6">
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <span class="blog-navigation">
                    <a><<</a>
                    <a><</a>
                    <a class="active">1</a>
                    <a>2</a>
                    <a>3</a>
                    <a>4</a>
                    <a>5</a>
                    <a>6</a>
                    <a>></a>
                    <a>>></a>
                </span>
            </div>
        </div>
        <div class="row blog-row">
            <div class="col-md-6 blog-block"><div class="thumbnail-blog">
                    <img src="../assets/images/blog/prvy.png"/>
                </div>
                <div class="blog-nazov">
                    Lorem ipsum dolor it set
                </div>
                <div class="description-blog">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nemo enim ipsam voluptatem quia voluptas sit asper
                    natur aut odit aut fugit, sed quia consequuntur magndolores eos qui ratione voluptatem sequi nesciunt.</div>

                <div class="read-more-blog"><a  href="blog_detail.php">Chci prečíst</a></div></div>
            <div class="col-md-6 blog-block"><div class="thumbnail-blog">
                    <img src="../assets/images/blog/druhy.png"/>
                </div>
                <div class="blog-nazov">
                    Lorem ipsum dolor it set
                </div>
                <div class="description-blog">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nemo enim ipsam voluptatem quia voluptas sit asper
                    natur aut odit aut fugit, sed quia consequuntur magndolores eos qui ratione voluptatem sequi nesciunt.</div>

                <div class="read-more-blog"><a  href="blog_detail.php">Chci prečíst</a></div>
            </div>
            <div class="col-md-6 blog-block"><div class="thumbnail-blog">
                    <img src="../assets/images/blog/prvy.png"/>
                </div>
                <div class="blog-nazov">
                    Lorem ipsum dolor it set
                </div>
                <div class="description-blog">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nemo enim ipsam voluptatem quia voluptas sit asper
                    natur aut odit aut fugit, sed quia consequuntur magndolores eos qui ratione voluptatem sequi nesciunt.</div>

                <div class="read-more-blog"><a  href="blog_detail.php">Chci prečíst</a></div></div>
            <div class="col-md-6 blog-block"><div class="thumbnail-blog">
                    <img src="../assets/images/blog/druhy.png"/>
                </div>
                <div class="blog-nazov">
                    Lorem ipsum dolor it set
                </div>
                <div class="description-blog">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nemo enim ipsam voluptatem quia voluptas sit asper
                    natur aut odit aut fugit, sed quia consequuntur magndolores eos qui ratione voluptatem sequi nesciunt.</div>

                <div class="read-more-blog"><a  href="blog_detail.php">Chci prečíst</a></div>
            </div>
            <div class="col-md-6 blog-block"><div class="thumbnail-blog">
                    <img src="../assets/images/blog/prvy.png"/>
                </div>
                <div class="blog-nazov">
                    Lorem ipsum dolor it set
                </div>
                <div class="description-blog">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nemo enim ipsam voluptatem quia voluptas sit asper
                    natur aut odit aut fugit, sed quia consequuntur magndolores eos qui ratione voluptatem sequi nesciunt.</div>

                <div class="read-more-blog"><a  href="blog_detail.php">Chci prečíst</a></div></div>
            <div class="col-md-6 blog-block"><div class="thumbnail-blog">
                    <img src="../assets/images/blog/druhy.png"/>
                </div>
                <div class="blog-nazov">
                    Lorem ipsum dolor it set
                </div>
                <div class="description-blog">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nemo enim ipsam voluptatem quia voluptas sit asper
                    natur aut odit aut fugit, sed quia consequuntur magndolores eos qui ratione voluptatem sequi nesciunt.</div>

                <div class="read-more-blog"><a  href="blog_detail.php">Chci prečíst</a></div>
            </div>
            <div class="col-md-6 blog-block"><div class="thumbnail-blog">
                    <img src="../assets/images/blog/prvy.png"/>
                </div>
                <div class="blog-nazov">
                    Lorem ipsum dolor it set
                </div>
                <div class="description-blog">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nemo enim ipsam voluptatem quia voluptas sit asper
                    natur aut odit aut fugit, sed quia consequuntur magndolores eos qui ratione voluptatem sequi nesciunt.</div>

                <div class="read-more-blog"><a href="blog_detail.php">Chci prečíst</a></div></div>
            <div class="col-md-6 blog-block"><div class="thumbnail-blog">
                    <img src="../assets/images/blog/druhy.png"/>
                </div>
                <div class="blog-nazov">
                    Lorem ipsum dolor it set
                </div>
                <div class="description-blog">Lorem ipsum doloOr sit amet, consectetuer adipiscing elit. Nemo enim ipsam voluptatem quia voluptas sit asper
                    natur aut odit aut fugit, sed quia consequuntur magndolores eos qui ratione voluptatem sequi nesciunt.</div>

                <div class="read-more-blog"><a href="blog_detail.php">Chci prečíst</a></div>
            </div>
        </div>
        <br/>
        <div class="row blog-row">
            <div class="col-md-4 col-sm-4 col-xs-12"></div>
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="viac-tlacitko2"><a target="_blank" href="#">Dalších 8 článkov</a></div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
                <span class="blog-navigation">
                    <a><<</a>
                    <a><</a>
                    <a class="active">1</a>
                    <a>2</a>
                    <a>3</a>
                    <a>4</a>
                    <a>5</a>
                    <a>6</a>
                    <a>></a>
                    <a>>></a>
                </span>
            </div>
        </div>
    </div>
</div>
<?php include('footer.php') ?>