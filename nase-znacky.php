<?php include('header.php') ?>
<div class="our-brands">

    <?php include('brand.php') ?>
    <div class="row">
        <div class="container-full">
            <div class="col-md-12" style="padding:0">
                <div class="row our-brands">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="hl-title">
                            <h2 ><span class="brand-title">GSI OUTDOORS</span></h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-7 col-sm-12 col-xs-12">
                        <img class="brand-images" src="../assets/images/gsi-outdoors-brand.png" />
                    </div>

                    <div class="col-md-5 col-sm-12 col-xs-12">
                        <div class="text-vse-zacalo">
                            Vše začalo před mnoha a mnoha lety u francouzsko-kanadských lovců a slušného množství podomácku vyrobeného alkoholu. Abychom tento příběh zkrátili, ráno po nich zůstalo jen pár žhavých uhlíků z táboráku, kus rozbahněné trávy polité kafem, několik rozedraných matrací a velmi, velmi nepohodlné tábořiště, silně zapáchající bourbonem. Brzy poté, více a více lidí opouštělo své jinak pohodlné domovy ve starém světě a vyráželi podél východní hranice oceánu za novými zážitky do divočiny. V průběhu dalších dvou století byl odkaz bláznivých lovců a dobrodruhů naplněn i na západě, ale je to právě trvalé dědictví krásy života mimo město a civilizaci, co nás v dnešní době nutí vyrazit ven.
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="gsi-logo"><img class="center-image" src="../assets/images/brand2.png"></div>
                    <div class="container">
                        <div class="text-pod-logom">Roku 1985, kdy v podobném duchu jako kdysi lovce hnala touha po dobrodružství, vyrazila skupinka sourozenců dolů podél západního pobřeží do San Diega, kde založili GSI Outdoors. Začínali s několika kousky smaltovaného nádobí povrchní znalostí táborového vybavení. Brzy však začala firma růst a věnovat se designu, výrobě, obchodu a marketingu, stále většího množství outdoorového nádobí, jídelních pomůcek a příslušenství. Po nějaké době se Don, Ian a Kathy Scott rozhodli přesunout sídlo do Spokane, stát Washington, kde vyrůstali. Dnes jsou výrobky GSI Outdoors dostupné po celém světě a značka GSI proslula skvělou kvalitou, technickými parametry a moderním designem svých výrobků, především ale humorným a zábavných přístupem majitelů k outdooru.</div>
                        <div class="col-md-12">
                            <div class="gsi-obrazok-pod-logom"><img src="../assets/images/obrazky-pod-logom.png"></div>
                            <div class="text-pod-obrazkom2">V GSI Outdoors se nesnažíme přinést na trh převratné novinky díky nějakému „novému zázračnému materiálu“, ani díky monumentálnímu tlačení nějaké nepředstavitelné vychytávky. Ne. My považujeme za inovaci stvoření unikátního, leč představitelného a hmatatelného produktu, který odpovídá našim představám a respektuje hodnoty, které sdílíme. Jsme velmi hrdi na to, že se nám podařilo vytvořit nové a zajímavé věcičky z toho, co ostatní považovali za nemožné, nebo to nestálo za jejich námahu. Víme, že tento přístup si můžeme zachovat jen díky našim zákazníkům, kteří jsou cenným zdrojem inspirace.<br /><br />

                                GSI Outdoors je malá rodinná společnost. Nemáme žádné závazky k anonymní skupině akcionářů, představenstvu, nebo komukoliv jinému, vyjma našich zákazníků, obchodních partnerů a vůči sobě samým. Je to svoboda, která ztělesňuje outdoor, a dává nám volnost reagovat na potřeby našich zákazníků, kteří milují pohyb v přírodě, stejně tak, jako my.






                            </div>
                            <div class="technologie">
                                <h3>Technológie</h3>

                            </div>
                            <div class="col-md-3"><div class="halulite"></div></div>
                            <div class="col-md-3"><div class="halulite"></div></div>
                            <div class="col-md-3"><div class="halulite"></div></div>
                            <div class="col-md-3"><div class="halulite"></div></div>
                            <div class="col-md-3"><div class="halulite"></div></div>
                            <div class="col-md-3"><div class="halulite"></div></div>
                            <div class="col-md-3"><div class="halulite"></div></div>
                            <div class="col-md-3"><div class="halulite"></div></div>
                            <div class="text-pod-halulite">Nádobí z materiálu Halulite se vyznačuje lehkostí, houževnatostí proti poškrábání a odření titanu, ale oproti titanu si zachovává výborné vlastnosti při vedení tepla a tím zkracuje dobu vaření a šetří tak palivo. Řada Halilite dává sbohem škrábancům, propáleninám a zkroucení nádobí. Jedná se o ultralehké nádobí bez kompromisů. Toto nádobí nepotřebuje žádnou další povrchovou úpravu, což ještě podtrhuje lehkost nádobí Halulite. Nádobí z řady Halulite je optimální volbou pro horolezce a alpinisty.</div>

                            <div class="system-skladani">
                                <h3>Systém skladaní</h3>

                            </div>
                            <div class="col-md-4"><div class="skladani1"><img src="../assets/images/skladani1.png"/></div></div>
                            <div class="col-md-4"><div class="skladani2"><img src="../assets/images/skladani2.png"/></div></div>
                            <div class="col-md-4"><div class="skladani3"><img src="../assets/images/skladani3.png"/></div></div>

                            <div class="col-md-12"><div class="skladani-text">
                                    Universální systém pro outdoorové vaření kombinující velikost, hmotnost a výborné vlastnosti nádobí. Tato řada je určena pro turistiku, autoturistiku, kempování či vodáctví. Bylo nebylo, v dřívějších dobách se gurmánství a kempink nedali lehce spojit dohromady. A tak se stávalo, že gurmáni holdující také vášni života v divočině většinou trpěli. To utrpení bylo o to hladovější, o co více museli dělat kompromisy ve svém kuchařském vybavení… nFORM Crossover systém reprezentuje ideální řešení pro každého, kdo na dlouhé cestě touží po bohatém jídelníčku, aniž by musel vláčet celou svoji domácí kuchyňskou výbavu… Navíc velikost tohoto systému lze nastavit dle potřeby na nošení v batohu, či kempování nebo zkrátka něco mezi tím. Přidáváním, nebo odebíráním jednotlivých dílů tak lze tento set nastavit pro 2 až 4 osoby, nebo každého, kdo má chuť užít pohodlné stolování na cestách.. Ultralight je jednoduše osvícením moderního pobytu
                                    v přírodě…
                                </div></div>
                        </div>
                        <div class="col-md-12 nase-znacky-top-produkty"><?php include ('top-produkty.php') ?></div>
                    </div>
                </div>

            </div>
        </div>

    </div>
    <?php include('footer.php') ?>
</div>
