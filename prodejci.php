<?php include ('header.php') ?>

<div class="row">
	<div class="container">
		<div class="col-md-12">
			 <div class="hl-title">
				<h2><span>PRODEJCI</span></h2>
			</div>
            <br/>
            <br/>
			<div class="text-prodejci">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </div>
		
<div class="mapa">
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2525.4785793709157!2d15.450875415652987!3d50.72961197951489!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x470ec67a8e833e07%3A0x85283acffea3493b!2sHorn%C3%AD+Rokytnice+470%2C+512+45+Rokytnice+nad+Jizerou%2C+%C4%8Cesko!5e0!3m2!1ssk!2ssk!4v1512396119558" style="padding-left: 120px;padding-right: 40px; border: 0;" allowfullscreen="" width="100%" height="550px" frameborder="0"></iframe>
	</div>
    <div class="logo-prodejci">

        <img src="assets/images/brand1.png" />


        <img src="assets/images/brand2.png" />


        <img src="assets/images/brand3.png" />


        <img src="assets/images/brand4.png" />


        <img src="assets/images/brand5.png" />


        <img src="assets/images/brand6.png" />


        <img src="assets/images/brand7.png" />

        <img src="assets/images/brand8.png" />


        <img src="assets/images/brand9.png" />

    </div>
            <br/>
	<div class="volby">
<div class=" volby-block">
    <p  style="display:inline-block;">Najít dle kraje</p >&nbsp;<select class="volby2"><option>Výber</option></select>
</div>
<div class=" volby-block">
    <p style="display:inline-block;">Odborný prodejce OOPP</p>&nbsp;<select class="volby2"><option>Výber</option></select>
</div>
<div class=" volby-block">
    <p style="display:inline-block;">Revize OOPP</p> &nbsp;<select class="volby2"><option>Výber</option></select>
</div>
<div class="volby-block">
    <p style="display:inline-block;">Servisní stredisko Pleps</p>&nbsp;<select class="volby2"><option>Výber</option></select>
</div>
</div>

<div class="prodejci-blok">
<div class="nadpis-kraja">
	<h3>Jihomoravský kraj</h3>
</div>

<div class="col-md-3 info-prodejci">
	<div class="meno-predajcu">Jméno prodejcu</div>
	<div class="adresa-predajcu">Adresa</div>
	<div class="mesto-predajcu">Mesto psč</div>
	<div class="webovy-odkaz-predajcu"><a target="_blank" href="#">webový odkaz</a></div>
	<div class="odkaz-na-pin-v-mape"><a target="_blank" href="#">odkaz na pin v mape</a></div>
	<div class="otviraci-doba">Otvrírací doba:</div>
	<div class="cas-otviraci-doba">Po-Pá 7:00 - 19:00 <br />Sob: 8:00 - 13:00</div>
	<br />
	<div class="meno-predajcu">Jméno prodejcu</div>
	<div class="adresa-predajcu">Adresa</div>
	<div class="mesto-predajcu">Mesto psč</div>
	<div class="webovy-odkaz-predajcu"><a target="_blank" href="#">webový odkaz</a></div>
	<div class="odkaz-na-pin-v-mape"><a target="_blank" href="#">odkaz na pin v mape</a></div>
	<div class="otviraci-doba">Otvrírací doba:</div>
	<div class="cas-otviraci-doba">Po-Pá 7:00 - 19:00 <br />Sob: 8:00 - 13:00</div>
</div>
<div class="col-md-3 info-prodejci2">
	<div class="meno-predajcu">Jméno prodejcu</div>
	<div class="adresa-predajcu">Adresa</div>
	<div class="mesto-predajcu">Mesto psč</div>
	<div class="webovy-odkaz-predajcu"><a target="_blank" href="#">webový odkaz</a></div>
	<div class="odkaz-na-pin-v-mape"><a target="_blank" href="#">odkaz na pin v mape</a></div>
	<div class="otviraci-doba">Otvrírací doba:</div>
	<div class="cas-otviraci-doba">Po-Pá 7:00 - 19:00 <br />Sob: 8:00 - 13:00</div>
		<br />
	<div class="meno-predajcu">Jméno prodejcu</div>
	<div class="adresa-predajcu">Adresa</div>
	<div class="mesto-predajcu">Mesto psč</div>
	<div class="webovy-odkaz-predajcu"><a target="_blank" href="#">webový odkaz</a></div>
	<div class="odkaz-na-pin-v-mape"><a target="_blank" href="#">odkaz na pin v mape</a></div>
	<div class="otviraci-doba">Otvrírací doba:</div>
	<div class="cas-otviraci-doba">Po-Pá 7:00 - 19:00 <br />Sob: 8:00 - 13:00</div>
</div>

<div class="col-md-3 info-prodejci2">
	<div class="meno-predajcu">Jméno prodejcu</div>
	<div class="adresa-predajcu">Adresa</div>
	<div class="mesto-predajcu">Mesto psč</div>
	<div class="webovy-odkaz-predajcu"><a target="_blank" href="#">webový odkaz</a></div>
	<div class="odkaz-na-pin-v-mape"><a target="_blank" href="#">odkaz na pin v mape</a></div>
	<div class="otviraci-doba">Otvrírací doba:</div>
	<div class="cas-otviraci-doba">Po-Pá 7:00 - 19:00 <br />Sob: 8:00 - 13:00</div>
		<br />
	<div class="meno-predajcu">Jméno prodejcu</div>
	<div class="adresa-predajcu">Adresa</div>
	<div class="mesto-predajcu">Mesto psč</div>
	<div class="webovy-odkaz-predajcu"><a target="_blank" href="#">webový odkaz</a></div>
	<div class="odkaz-na-pin-v-mape"><a target="_blank" href="#">odkaz na pin v mape</a></div>
	<div class="otviraci-doba">Otvrírací doba:</div>
	<div class="cas-otviraci-doba">Po-Pá 7:00 - 19:00 <br />Sob: 8:00 - 13:00</div>
</div>

<div class="col-md-3 info-prodejci3">
	<div class="meno-predajcu">Jméno prodejcu</div>
	<div class="adresa-predajcu">Adresa</div>
	<div class="mesto-predajcu">Mesto psč</div>
	<div class="webovy-odkaz-predajcu"><a target="_blank" href="#">webový odkaz</a></div>
	<div class="odkaz-na-pin-v-mape"><a target="_blank" href="#">odkaz na pin v mape</a></div>
	<div class="otviraci-doba">Otvrírací doba:</div>
	<div class="cas-otviraci-doba">Po-Pá 7:00 - 19:00 <br />Sob: 8:00 - 13:00</div>
		<br />
	<div class="meno-predajcu">Jméno prodejcu</div>
	<div class="adresa-predajcu">Adresa</div>
	<div class="mesto-predajcu">Mesto psč</div>
	<div class="webovy-odkaz-predajcu"><a target="_blank" href="#">webový odkaz</a></div>
	<div class="odkaz-na-pin-v-mape"><a target="_blank" href="#">odkaz na pin v mape</a></div>
	<div class="otviraci-doba">Otvrírací doba:</div>
	<div class="cas-otviraci-doba">Po-Pá 7:00 - 19:00 <br />Sob: 8:00 - 13:00</div>
</div></div>
<br /><br />
<div class="prodejci-blok">
<div class="nadpis-kraja"><br /><br />
	<h3>Jihomoravský kraj</h3>
</div>

<div class="col-md-3 info-prodejci">
	<div class="meno-predajcu">Jméno prodejcu</div>
	<div class="adresa-predajcu">Adresa</div>
	<div class="mesto-predajcu">Mesto psč</div>
	<div class="webovy-odkaz-predajcu"><a target="_blank" href="#">webový odkaz</a></div>
	<div class="odkaz-na-pin-v-mape"><a target="_blank" href="#">odkaz na pin v mape</a></div>
	<div class="otviraci-doba">Otvrírací doba:</div>
	<div class="cas-otviraci-doba">Po-Pá 7:00 - 19:00 <br />Sob: 8:00 - 13:00</div>
	<br />
	<div class="meno-predajcu">Jméno prodejcu</div>
	<div class="adresa-predajcu">Adresa</div>
	<div class="mesto-predajcu">Mesto psč</div>
	<div class="webovy-odkaz-predajcu"><a target="_blank" href="#">webový odkaz</a></div>
	<div class="odkaz-na-pin-v-mape"><a target="_blank" href="#">odkaz na pin v mape</a></div>
	<div class="otviraci-doba">Otvrírací doba:</div>
	<div class="cas-otviraci-doba">Po-Pá 7:00 - 19:00 <br />Sob: 8:00 - 13:00</div>
</div>
<div class="col-md-3 info-prodejci2">
	<div class="meno-predajcu">Jméno prodejcu</div>
	<div class="adresa-predajcu">Adresa</div>
	<div class="mesto-predajcu">Mesto psč</div>
	<div class="webovy-odkaz-predajcu"><a target="_blank" href="#">webový odkaz</a></div>
	<div class="odkaz-na-pin-v-mape"><a target="_blank" href="#">odkaz na pin v mape</a></div>
	<div class="otviraci-doba">Otvrírací doba:</div>
	<div class="cas-otviraci-doba">Po-Pá 7:00 - 19:00 <br />Sob: 8:00 - 13:00</div>
		<br />
	<div class="meno-predajcu">Jméno prodejcu</div>
	<div class="adresa-predajcu">Adresa</div>
	<div class="mesto-predajcu">Mesto psč</div>
	<div class="webovy-odkaz-predajcu"><a target="_blank" href="#">webový odkaz</a></div>
	<div class="odkaz-na-pin-v-mape"><a target="_blank" href="#">odkaz na pin v mape</a></div>
	<div class="otviraci-doba">Otvrírací doba:</div>
	<div class="cas-otviraci-doba">Po-Pá 7:00 - 19:00 <br />Sob: 8:00 - 13:00</div>
</div>

<div class="col-md-3 info-prodejci2">
	<div class="meno-predajcu">Jméno prodejcu</div>
	<div class="adresa-predajcu">Adresa</div>
	<div class="mesto-predajcu">Mesto psč</div>
	<div class="webovy-odkaz-predajcu"><a target="_blank" href="#">webový odkaz</a></div>
	<div class="odkaz-na-pin-v-mape"><a target="_blank" href="#">odkaz na pin v mape</a></div>
	<div class="otviraci-doba">Otvrírací doba:</div>
	<div class="cas-otviraci-doba">Po-Pá 7:00 - 19:00 <br />Sob: 8:00 - 13:00</div>
		<br />
	<div class="meno-predajcu">Jméno prodejcu</div>
	<div class="adresa-predajcu">Adresa</div>
	<div class="mesto-predajcu">Mesto psč</div>
	<div class="webovy-odkaz-predajcu"><a target="_blank" href="#">webový odkaz</a></div>
	<div class="odkaz-na-pin-v-mape"><a target="_blank" href="#">odkaz na pin v mape</a></div>
	<div class="otviraci-doba">Otvrírací doba:</div>
	<div class="cas-otviraci-doba">Po-Pá 7:00 - 19:00 <br />Sob: 8:00 - 13:00</div>
</div>

<div class="col-md-3 info-prodejci3">
	<div class="meno-predajcu">Jméno prodejcu</div>
	<div class="adresa-predajcu">Adresa</div>
	<div class="mesto-predajcu">Mesto psč</div>
	<div class="webovy-odkaz-predajcu"><a target="_blank" href="#">webový odkaz</a></div>
	<div class="odkaz-na-pin-v-mape"><a target="_blank" href="#">odkaz na pin v mape</a></div>
	<div class="otviraci-doba">Otvrírací doba:</div>
	<div class="cas-otviraci-doba">Po-Pá 7:00 - 19:00 <br />Sob: 8:00 - 13:00</div>
		<br />
	<div class="meno-predajcu">Jméno prodejcu</div>
	<div class="adresa-predajcu">Adresa</div>
	<div class="mesto-predajcu">Mesto psč</div>
	<div class="webovy-odkaz-predajcu"><a target="_blank" href="#">webový odkaz</a></div>
	<div class="odkaz-na-pin-v-mape"><a target="_blank" href="#">odkaz na pin v mape</a></div>
	<div class="otviraci-doba">Otvrírací doba:</div>
	<div class="cas-otviraci-doba">Po-Pá 7:00 - 19:00 <br />Sob: 8:00 - 13:00</div>
</div>
</div>
<br /><br />
<div class="prodejci-blok">
<div class="nadpis-kraja">
	<br /><br />
	<h3>Jihomoravský kraj</h3>
</div>

<div class="col-md-3 info-prodejci">
	<div class="meno-predajcu">Jméno prodejcu</div>
	<div class="adresa-predajcu">Adresa</div>
	<div class="mesto-predajcu">Mesto psč</div>
	<div class="webovy-odkaz-predajcu"><a target="_blank" href="#">webový odkaz</a></div>
	<div class="odkaz-na-pin-v-mape"><a target="_blank" href="#">odkaz na pin v mape</a></div>
	<div class="otviraci-doba">Otvrírací doba:</div>
	<div class="cas-otviraci-doba">Po-Pá 7:00 - 19:00 <br />Sob: 8:00 - 13:00</div>
	<br />
	<div class="meno-predajcu">Jméno prodejcu</div>
	<div class="adresa-predajcu">Adresa</div>
	<div class="mesto-predajcu">Mesto psč</div>
	<div class="webovy-odkaz-predajcu"><a target="_blank" href="#">webový odkaz</a></div>
	<div class="odkaz-na-pin-v-mape"><a target="_blank" href="#">odkaz na pin v mape</a></div>
	<div class="otviraci-doba">Otvrírací doba:</div>
	<div class="cas-otviraci-doba">Po-Pá 7:00 - 19:00 <br />Sob: 8:00 - 13:00</div>
</div>
<div class="col-md-3 info-prodejci2">
	<div class="meno-predajcu">Jméno prodejcu</div>
	<div class="adresa-predajcu">Adresa</div>
	<div class="mesto-predajcu">Mesto psč</div>
	<div class="webovy-odkaz-predajcu"><a target="_blank" href="#">webový odkaz</a></div>
	<div class="odkaz-na-pin-v-mape"><a target="_blank" href="#">odkaz na pin v mape</a></div>
	<div class="otviraci-doba">Otvrírací doba:</div>
	<div class="cas-otviraci-doba">Po-Pá 7:00 - 19:00 <br />Sob: 8:00 - 13:00</div>
		<br />
	<div class="meno-predajcu">Jméno prodejcu</div>
	<div class="adresa-predajcu">Adresa</div>
	<div class="mesto-predajcu">Mesto psč</div>
	<div class="webovy-odkaz-predajcu"><a target="_blank" href="#">webový odkaz</a></div>
	<div class="odkaz-na-pin-v-mape"><a target="_blank" href="#">odkaz na pin v mape</a></div>
	<div class="otviraci-doba">Otvrírací doba:</div>
	<div class="cas-otviraci-doba">Po-Pá 7:00 - 19:00 <br />Sob: 8:00 - 13:00</div>
</div>

<div class="col-md-3 info-prodejci2">
	<div class="meno-predajcu">Jméno prodejcu</div>
	<div class="adresa-predajcu">Adresa</div>
	<div class="mesto-predajcu">Mesto psč</div>
	<div class="webovy-odkaz-predajcu"><a target="_blank" href="#">webový odkaz</a></div>
	<div class="odkaz-na-pin-v-mape"><a target="_blank" href="#">odkaz na pin v mape</a></div>
	<div class="otviraci-doba">Otvrírací doba:</div>
	<div class="cas-otviraci-doba">Po-Pá 7:00 - 19:00 <br />Sob: 8:00 - 13:00</div>
		<br />
	<div class="meno-predajcu">Jméno prodejcu</div>
	<div class="adresa-predajcu">Adresa</div>
	<div class="mesto-predajcu">Mesto psč</div>
	<div class="webovy-odkaz-predajcu"><a target="_blank" href="#">webový odkaz</a></div>
	<div class="odkaz-na-pin-v-mape"><a target="_blank" href="#">odkaz na pin v mape</a></div>
	<div class="otviraci-doba">Otvrírací doba:</div>
	<div class="cas-otviraci-doba">Po-Pá 7:00 - 19:00 <br />Sob: 8:00 - 13:00</div>
</div>

<div class="col-md-3 info-prodejci3">
	<div class="meno-predajcu">Jméno prodejcu</div>
	<div class="adresa-predajcu">Adresa</div>
	<div class="mesto-predajcu">Mesto psč</div>
	<div class="webovy-odkaz-predajcu"><a target="_blank" href="#">webový odkaz</a></div>
	<div class="odkaz-na-pin-v-mape"><a target="_blank" href="#">odkaz na pin v mape</a></div>
	<div class="otviraci-doba">Otvrírací doba:</div>
	<div class="cas-otviraci-doba">Po-Pá 7:00 - 19:00 <br />Sob: 8:00 - 13:00</div>
		<br />
	<div class="meno-predajcu">Jméno prodejcu</div>
	<div class="adresa-predajcu">Adresa</div>
	<div class="mesto-predajcu">Mesto psč</div>
	<div class="webovy-odkaz-predajcu"><a target="_blank" href="#">webový odkaz</a></div>
	<div class="odkaz-na-pin-v-mape"><a target="_blank" href="#">odkaz na pin v mape</a></div>
	<div class="otviraci-doba">Otvrírací doba:</div>
	<div class="cas-otviraci-doba">Po-Pá 7:00 - 19:00 <br />Sob: 8:00 - 13:00</div>
</div>
		</div>

		</div>
	</div>
</div>

<?php include ('footer.php') ?>