<?php include('header.php') ?>
<div class="row">
	<div class="container">
		<div class="col-md-12">
			<div class="hl-title-page">
				<h2><span>Kontakty</span></h2></div>
			</div>

<div class="row">
    <div class="col-md-12">
        <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="zakladne-informacie"><p>Základní informace</p></div>
            <div class="sidlo"><p>Sídlo společnosti:</p></div>
            <div class="adresa-kontakt"><p>
                    TOTAL Outdoor s.r.o.
                    <br />
                    Fabiánová 1275/6, 150 00 Praha 5
                    <br/>
                    <span class="email-kontakt"><i class="fa fa-envelope-o" aria-hidden="true"></i> info@totaloutdoor.cz</span>
                </p>
            </div>
            <div class="ico-dic">
                <p>IČO: 26451875 <br />
                    DIČ: CZ26451875</p>
            </div>
            <div class="zapis"><p>
                    Zapsaná v Obchodním rejstříku vedeném Městským soudem v Praze,<br />
                    oddíl C, vložka 83191. Zastoupená jednateli společnosti: <br />
                    Ing. Karlem Mandíkem a Janem Prokešem.

                </p></div>

            <div class="objednavky"><p>Objednávky</p></div>
            <div class="objednavky-email"><p><i class="fa fa-envelope-o" aria-hidden="true"></i> objednavky@totaloutdoor.cz</p></div>
        </div>

        <div class="col-md-6 col-xs-12 col-sm-12">

            <div class="row contact-data">

                <div class="col-md-12 col-sm-12 col-xs-12 avatar contact">
                    <div class="kontaktni-osoby"><p>Kontaktní osoby:</p></div>

                    <div class="col-md-2 "><div class="fotka-kontakt"><img src="../assets/images/avatar.png" /></div></div><div class="col-md-5">

                        <div class="meno-kontakt">
                            <p>Ján Prokeš</p></div>
                        <div class="funkcia">
                            <p>obchod</p></div>
                        <div class="email-osoba">
                            <p><i class="fa fa-envelope-o" aria-hidden="true"></i> honza@totaloutdoor.cz</p></div>
                        <div class="telefon-osoba">
                            <p><i class="fa fa-phone" aria-hidden="true"></i> +420 773 491 880</p></div>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12 avatar contact"><div class="col-md-2"><div class="fotka-kontakt"><img src="../assets/images/avatar.png" /></div></div><div class="col-md-5">

                        <div class="meno-kontakt">
                            <p>Karel Mandík </p></div>
                        <div class="funkcia">
                            <p>marketing, finance</p></div>
                        <div class="email-osoba">
                            <p><i class="fa fa-envelope-o" aria-hidden="true"></i> karel@totaloutdoor.cz</p></div>
                        <div class="telefon-osoba">
                            <p><i class="fa fa-phone" aria-hidden="true"></i> +420 725 606 300</p></div>
                    </div></div>
                <div class="col-md-12 col-sm-12 col-xs-12 avatar contact"><div class="col-md-2"><div class="fotka-kontakt"><img src="../assets/images/avatar.png" /></div></div><div class="col-md-5">

                        <div class="meno-kontakt">
                            <p>Ján Prokeš</p></div>
                        <div class="funkcia">
                            <p>obchod</p></div>
                        <div class="email-osoba">
                            <p><i class="fa fa-envelope-o" aria-hidden="true"></i> honza@totaloutdoor.cz</p></div>
                        <div class="telefon-osoba">
                            <p><i class="fa fa-phone" aria-hidden="true"></i> +420 773 491 880</p></div>
                    </div></div>
                <div class="col-md-12 col-sm-12 col-xs-12 avatar contact"><div class="col-md-2"><div class="fotka-kontakt"><img src="../assets/images/avatar.png" /></div></div><div class="col-md-5">

                        <div class="meno-kontakt">
                            <p>Ján Prokeš</p></div>
                        <div class="funkcia">
                            <p>obchod</p></div>
                        <div class="email-osoba">
                            <p><i class="fa fa-envelope-o" aria-hidden="true"></i> honza@totaloutdoor.cz</p></div>
                        <div class="telefon-osoba">
                            <p><i class="fa fa-phone" aria-hidden="true"></i> +420 773 491 880</p></div>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12 avatar posledny-block contact"><div class="col-md-2"><div class="fotka-kontakt"><img src="../assets/images/avatar.png" /></div></div><div class="col-md-5">

                        <div class="meno-kontakt">
                            <p>Ján Prokeš</p></div>
                        <div class="funkcia">
                            <p>obchod</p></div>
                        <div class="email-osoba">
                            <p><i class="fa fa-envelope-o" aria-hidden="true"></i> honza@totaloutdoor.cz</p></div>
                        <div class="telefon-osoba">
                            <p><i class="fa fa-phone" aria-hidden="true"></i> +420 773 491 880</p></div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-6 col-sm-12 col-xs-12" style="padding:0">
            <div class="velkoobchod"><p>Veľkoobchodní sklad</p></div>
            <div class="velkoobchod-adresa"><p>Horní Rokytnice 470 <br />
                    512 44 Rokytnice nad Jizerou </p></div>
        </div>
    </div>
	<div class="col-md-12" style="padding:0">
		<div class="mapa contact-map">
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2525.4785793709157!2d15.450875415652987!3d50.72961197951489!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x470ec67a8e833e07%3A0x85283acffea3493b!2sHorn%C3%AD+Rokytnice+470%2C+512+45+Rokytnice+nad+Jizerou%2C+%C4%8Cesko!5e0!3m2!1ssk!2ssk!4v1512396119558" width="100%" height="350px" frameborder="0" style="border:0" allowfullscreen></iframe>
	</div></div></div>
	</div>

	</div>
<?php include('footer.php') ?>