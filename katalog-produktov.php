
  <div class="row">
	<style>
	@media (max-width: 450px){
		.container-fluid,
		.container-fluid > .col-md-12{
			padding-right:0px;
			padding-left:0px;
		}
		.image-box > img{
			max-width:100%;
		}
		.item > .col-sm-6{
			padding-left:0px;
			padding-right:0px;
		}
		.pridatdokosika{
			max-width:100%;
		}
		.product-item{
			max-width:98% !important;
			margin-top:5%;
		}
		.item > .col-xs-6:nth-child(odd) > .product-item {
			margin-right:5%;
			margin-left:0;
		}
		.item > .col-xs-6:nth-child(even) > .product-item {
			margin-left:3%;
			margin-right:4px;
		}
		.more-products{
			margin-bottom:15%;
		}
		.carousel-control{
			display:none;
		}
	}
	</style>
	<div class="container-full katalog-pozadie katalog-carousel">
	<div class="container-fluid"> 
		<div class="col-md-12">
			<div class="hl-title">
				<h2><span>Katalog produktů</span></h2></div>
                <div id="index-carousel" class="carousel slide" data-ride="carousel">

                    <div class="carousel-inner" role="listbox">

                        <div class="item active">
							
								<div class="col-md-3 col-sm-6 col-xs-6">
									<div class="product-item">
										<div class="image-box">
											<img src="../assets/images/produkt1.png"/>
										</div>
										<div class="nazov-produktu">Nazov produktu na 2 rádky</div>
										<div class="znacky">Outdoor</div>
										<div class="cena-produktu">98 765 Kč</div>
										<div class="pridatdokosika"><a target="_blank" href="#">Chci to koupit</a></div>
									</div>
								</div>
								<div class="col-md-3 col-sm-6 col-xs-6">
									<div class="product-item">
										<div class="image-box">
											<img src="../assets/images/produkt1.png"/>
										</div>
										<div class="nazov-produktu">Nazov produktu na 2 rádky</div>
										<div class="znacky">Outdoor</div>
										<div class="cena-produktu">98 765 Kč</div>
										<div class="pridatdokosika"><a target="_blank" href="#">Chci to koupit</a></div>
									</div>
								</div>
								<div class="col-md-3 col-sm-6 col-xs-6">
									<div class="product-item">
										<div class="image-box">
											<img src="../assets/images/produkt1.png"/>
										</div>
										<div class="nazov-produktu">Nazov produktu na 2 rádky</div>
										<div class="znacky">Outdoor</div>
										<div class="cena-produktu">98 765 Kč</div>
										<div class="pridatdokosika"><a target="_blank" href="#">Chci to koupit</a></div>
									</div>
								</div>
								<div class="col-md-3 col-sm-6 col-xs-6">
									<div class="product-item">
										<div class="image-box">
											<img src="../assets/images/produkt1.png"/>
										</div>
										<div class="nazov-produktu">Nazov produktu na 2 rádky</div>
										<div class="znacky">Outdoor</div>
										<div class="cena-produktu">98 765 Kč</div>
										<div class="pridatdokosika"><a target="_blank" href="#">Chci to koupit</a></div>
									</div>
								</div>
							
                        </div>

                        <div class="item ">
                            <div class="col-md-3 col-sm-6 col-xs-6">
                                <div class="product-item">
                                    <div class="image-box">
                                        <img src="../assets/images/produkt1.png"/>
                                    </div>
                                    <div class="nazov-produktu">Nazov produktu na 2 rádky</div>
                                    <div class="znacky">Outdoor</div>
                                    <div class="cena-produktu">98 765 Kč</div>
                                    <div class="pridatdokosika"><a target="_blank" href="#">Chci to koupit</a></div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-6">
                                <div class="product-item">
                                    <div class="image-box">
                                        <img src="../assets/images/produkt1.png"/>
                                    </div>
                                    <div class="nazov-produktu">Nazov produktu na 2 rádky</div>
                                    <div class="znacky">Outdoor</div>
                                    <div class="cena-produktu">98 765 Kč</div>
                                    <div class="pridatdokosika"><a target="_blank" href="#">Chci to koupit</a></div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-6">
                                <div class="product-item">
                                    <div class="image-box">
                                        <img src="../assets/images/produkt1.png"/>
                                    </div>
                                    <div class="nazov-produktu">Nazov produktu na 2 rádky</div>
                                    <div class="znacky">Outdoor</div>
                                    <div class="cena-produktu">98 765 Kč</div>
                                    <div class="pridatdokosika"><a target="_blank" href="#">Chci to koupit</a></div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-6">
                                <div class="product-item">
                                    <div class="image-box">
                                        <img src="../assets/images/produkt1.png"/>
                                    </div>
                                    <div class="nazov-produktu">Nazov produktu na 2 rádky</div>
                                    <div class="znacky">Outdoor</div>
                                    <div class="cena-produktu">98 765 Kč</div>
                                    <div class="pridatdokosika"><a target="_blank" href="#">Chci to koupit</a></div>
                                </div>
                            </div>
                        </div>

                        <div class="item">
                            <div class="col-md-3 col-sm-6 col-xs-6">
                                <div class="product-item">
                                    <div class="image-box">
                                        <img src="../assets/images/produkt1.png"/>
                                    </div>
                                    <div class="nazov-produktu">Nazov produktu na 2 rádky</div>
                                    <div class="znacky">Outdoor</div>
                                    <div class="cena-produktu">98 765 Kč</div>
                                    <div class="pridatdokosika"><a target="_blank" href="#">Chci to koupit</a></div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-6">
                                <div class="product-item">
                                    <div class="image-box">
                                        <img src="../assets/images/produkt1.png"/>
                                    </div>
                                    <div class="nazov-produktu">Nazov produktu na 2 rádky</div>
                                    <div class="znacky">Outdoor</div>
                                    <div class="cena-produktu">98 765 Kč</div>
                                    <div class="pridatdokosika"><a target="_blank" href="#">Chci to koupit</a></div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-6">
                                <div class="product-item">
                                    <div class="image-box">
                                        <img src="../assets/images/produkt1.png"/>
                                    </div>
                                    <div class="nazov-produktu">Nazov produktu na 2 rádky</div>
                                    <div class="znacky">Outdoor</div>
                                    <div class="cena-produktu">98 765 Kč</div>
                                    <div class="pridatdokosika"><a target="_blank" href="#">Chci to koupit</a></div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-6">
                                <div class="product-item">
                                    <div class="image-box">
                                        <img src="../assets/images/produkt1.png"/>
                                    </div>
                                    <div class="nazov-produktu">Nazov produktu na 2 rádky</div>
                                    <div class="znacky">Outdoor</div>
                                    <div class="cena-produktu">98 765 Kč</div>
                                    <div class="pridatdokosika"><a target="_blank" href="#">Chci to koupit</a></div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <!-- Controls -->
                    <a class="left carousel-control" href="#index-carousel" role="button" data-slide="prev">
                        <img src="assets/images/left-arrow.png"/>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#index-carousel" role="button" data-slide="next">
                        <img src="assets/images/arrow-right.png" />
                        <span class="sr-only">Next</span>
                    </a>
                </div>

                <br/>
                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-4 col-sm-12 col-xs-12">
                        <button class="btn btn-default more-products">
                            Více produktů
                        </button>
                    </div>
                </div>
		</div>

	</div>
	</div>

</div>