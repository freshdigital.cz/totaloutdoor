<?php include('header.php') ?>
<div class="category-stuff">
    <style>
    .category-carousel-control span{
        padding:1%;
        background-color: #cccc00 !important;
    }
    .category-carousel-control span::before{
        color:white !important;
        text-shadow: none !important;
    }
    @media all and (max-width:984px){
        .carousel-control.left{
            left:-40px !important;
        }
        .carousel-control.right{
            right:-40px !important;
        }
    }
    @media all and (max-width:993px){
        .category-title h2::before{
            left: -4px !important;
        }
        .category-title h2::after{
            right: -4px !important;
        }
    }
    @media all and (max-width:401px){
        .ikonka-kategorie{
            width:100% !important;
        }
    }
     .scroll-down-thing{
            display:none;
            -webkit-transition: display .3s ease-in;
            -moz-transition: display .3s ease-in;
            -ms-transition: display .3s ease-in;
            -o-transition: display .3s ease-in;
            transition: display .3s ease-in;
        }
        .filter-menu{
            -webkit-transition: display .3s ease-in;
            -moz-transition: display .3s ease-in;
            -ms-transition: display .3s ease-in;
            -o-transition: display .3s ease-in;
            transition: display .3s ease-in;
        }

</style>
<div class="row">
	
	<div class="container">
		<div class="col-md-12">
            <div class="col-md-12" style="padding-left:5%;padding-right:5%;">
                <div class="col-md-3 col-sm-6 col-xs-12 ikonka-kategorie" onclick="location.href='../category-2.php';"  style="cursor: pointer;margin-left: 0%;background-image: url('../assets/images/horolezectvi.png')">
                <span class="description-homepage">
                    horolezectví
                </span>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12 ikonka-kategorie" onclick="location.href='../category-2.php';"  style="cursor: pointer; background-image:url('../assets/images/prace-ve-vyskach.png');background-size: cover;background-position: 80%;">
                <span class="description-homepage">
                    práce ve výškach
                </span>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12 ikonka-kategorie" onclick="location.href='../category-2.php';"  style="cursor: pointer; background-image: url('../assets/images/camping.png');background-size: cover;
    background-position: 50% 75%;">
                <span class="description-homepage">
                    camping
                </span>
                </div>

                <div class="col-md-3 col-sm-6 col-xs-12 ikonka-kategorie" onclick="location.href='../category-2.php';"  style="cursor: pointer; background-image: url('../assets/images/beautiful-2297215_1920.png');background-size: cover">
                <span class="description-homepage">
                    dětské vybavení
                </span>
                </div>

                <div class="col-md-3 col-sm-6 col-xs-12 ikonka-kategorie" onclick="location.href='../category-2.php';"  style="cursor: pointer; background-image: url('../assets/images/dog-1557956_1920.jpg');background-size: cover;background-position: 100%;">
                <span class="description-homepage">
                    pro zvířata
                </span>
                </div>

            </div>

		</div>
    <div class="banner-kategorie"><img src="../assets/images/banner-kategorie.png" />
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-6">
                <div class="logo-kategorie"><img src="../assets/images/logo-kategorie.png" /></div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6">
                <div class="text-banner"><p>Lorem ipsum dolor sit amet.</p></div>
                <div class="tlacitko-banner2">Koupit teď</div>
            </div>
        </div>
    </div>
<div class="scroll-down-thing">
        <div class="scroll-down-menu">
            <div class="scroll-down-menu-item">
                <div class="row">
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <h4>Expressky karabiny</h4>
                    </div>
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <img class="scroll-down-image" src="../assets/images/produkt1.png"/>
                    </div>
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <p>
                            adfgadfhgafhadfjgadghadgshsdgfhasd
                        </p>
                    </div>
                </div>
            </div>
            <div class="scroll-down-menu-item">
                <div class="row">
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <h4>Expressky karabiny</h4>
                    </div>
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <img class="scroll-down-image" src="../assets/images/produkt1.png"/>
                    </div>
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <p>
                            adfgadfhgafhadfjgadghadgshsdgfhasd
                        </p>
                    </div>
                </div>
            </div>
            <div class="scroll-down-menu-item">
                <div class="row">
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <h4>Expressky karabiny</h4>
                    </div>
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <img class="scroll-down-image" src="../assets/images/produkt1.png"/>
                    </div>
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <p>
                            adfgadfhgafhadfjgadghadgshsdgfhasd
                        </p>
                    </div>
                </div>
            </div>
            <div class="scroll-down-menu-item">
                <div class="row">
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <h4>Expressky karabiny</h4>
                    </div>
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <img class="scroll-down-image" src="../assets/images/produkt1.png"/>
                    </div>
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <p>
                            adfgadfhgafhadfjgadghadgshsdgfhasd
                        </p>
                    </div>
                </div>
            </div>
            <div class="scroll-down-menu-item">
                <div class="row">
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <h4>Expressky karabiny</h4>
                    </div>
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <img class="scroll-down-image" src="../assets/images/produkt1.png"/>
                    </div>
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <p>
                            adfgadfhgafhadfjgadghadgshsdgfhasd
                        </p>
                    </div>
                </div>
            </div>
            <div class="scroll-down-menu-item">
                <div class="row">
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <h4>Expressky karabiny</h4>
                    </div>
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <img class="scroll-down-image" src="../assets/images/produkt1.png"/>
                    </div>
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <p>
                            adfgadfhgafhadfjgadghadgshsdgfhasd
                        </p>
                    </div>
                </div>
            </div>
            <div class="scroll-down-menu-item">
                <div class="row">
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <h4>Expressky karabiny</h4>
                    </div>
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <img class="scroll-down-image" src="../assets/images/produkt1.png"/>
                    </div>
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <p>
                            adfgadfhgafhadfjgadghadgshsdgfhasd
                        </p>
                    </div>
                </div>
            </div>
            <div class="scroll-down-menu-item">
                <div class="row">
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <h4>Expressky karabiny</h4>
                    </div>
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <img class="scroll-down-image" src="../assets/images/produkt1.png"/>
                    </div>
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <p>
                            adfgadfhgafhadfjgadghadgshsdgfhasd
                        </p>
                    </div>
                </div>
            </div>
            <div class="scroll-down-menu-item">
                <div class="row">
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <h4>Expressky karabiny</h4>
                    </div>
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <img class="scroll-down-image" src="../assets/images/produkt1.png"/>
                    </div>
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <p>
                            adfgadfhgafhadfjgadghadgshsdgfhasd
                        </p>
                    </div>
                </div>
            </div>


            </div>
            <div class="container" >
                <div class="row scroll-down-footer">

                    <div class="col-md-8 col-sm-8 col-xs-12 scroll-down-footer-item" style="padding-left:2% !important;border: 1px solid rgba(109, 187, 219, .9);">
                        <span>Značka: </span>
                        <img src="assets/images/brand1.png"/>
                        <img src="assets/images/brand2.png"/>
                        <img src="assets/images/brand3.png"/>
                        <img src="assets/images/brand4.png"/>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12 scroll-down-footer-item" >
                        <button class="btn btn-footer-scroll-down">
                            VÝPRODEJ
                        </button>
                    </div>

                </div>
            </div>
        </div>

<div class="col-md-12">
	<div class="hl-title category-title">
				<h2><span>Katalog produktů</span></h2>
			</div>
<div class="col-md-12">
<div class="nadpis-kategorie">
	<h3>Horolezectví</h3>
</div>
    <div id="horolezectvi-carousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <div class="col-md-12">

                    <div class="col-md-3">
                        <div class="product-item">
                            <div class="image-box">
                                <img src="../assets/images/produkt1.png">
                            </div>
                            <div class="nazov-produktu">Nazov produktu na 2 rádky</div>
                            <div class="znacky">Outdoor</div>
                            <div class="cena-produktu">98 765 Kč</div>
                            <div class="pridatdokosika"><a target="_blank" target="_blank" href="#">Chci to koupit</a></div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="product-item">
                            <div class="image-box">
                                <img src="../assets/images/produkt1.png">
                            </div>
                            <div class="nazov-produktu">Nazov produktu na 2 rádky</div>
                            <div class="znacky">Outdoor</div>
                            <div class="cena-produktu">98 765 Kč</div>
                            <div class="pridatdokosika"><a target="_blank" target="_blank" href="#">Chci to koupit</a></div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="product-item">
                            <div class="image-box">
                                <img src="../assets/images/produkt1.png">
                            </div>
                            <div class="nazov-produktu">Nazov produktu na 2 rádky</div>
                            <div class="znacky">Outdoor</div>
                            <div class="cena-produktu">98 765 Kč</div>
                            <div class="pridatdokosika"><a target="_blank" target="_blank" href="#">Chci to koupit</a></div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="product-item">
                            <div class="image-box">
                                <img src="../assets/images/produkt1.png">
                            </div>
                            <div class="nazov-produktu">Nazov produktu na 2 rádky</div>
                            <div class="znacky">Outdoor</div>
                            <div class="cena-produktu">98 765 Kč</div>
                            <div class="pridatdokosika"><a target="_blank" target="_blank" href="#">Chci to koupit</a></div>
                        </div>
                    </div>


                </div>
            </div>
            <div class="item ">
                <div class="col-md-12">

                    <div class="col-md-3">
                        <div class="product-item">
                            <div class="image-box">
                                <img src="../assets/images/produkt1.png">
                            </div>
                            <div class="nazov-produktu">Nazov produktu na 2 rádky</div>
                            <div class="znacky">Outdoor</div>
                            <div class="cena-produktu">98 765 Kč</div>
                            <div class="pridatdokosika"><a target="_blank" target="_blank" href="#">Chci to koupit</a></div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="product-item">
                            <div class="image-box">
                                <img src="../assets/images/produkt1.png">
                            </div>
                            <div class="nazov-produktu">Nazov produktu na 2 rádky</div>
                            <div class="znacky">Outdoor</div>
                            <div class="cena-produktu">98 765 Kč</div>
                            <div class="pridatdokosika"><a target="_blank" target="_blank" href="#">Chci to koupit</a></div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="product-item">
                            <div class="image-box">
                                <img src="../assets/images/produkt1.png">
                            </div>
                            <div class="nazov-produktu">Nazov produktu na 2 rádky</div>
                            <div class="znacky">Outdoor</div>
                            <div class="cena-produktu">98 765 Kč</div>
                            <div class="pridatdokosika"><a target="_blank" target="_blank" href="#">Chci to koupit</a></div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="product-item">
                            <div class="image-box">
                                <img src="../assets/images/produkt1.png">
                            </div>
                            <div class="nazov-produktu">Nazov produktu na 2 rádky</div>
                            <div class="znacky">Outdoor</div>
                            <div class="cena-produktu">98 765 Kč</div>
                            <div class="pridatdokosika"><a target="_blank" target="_blank" href="#">Chci to koupit</a></div>
                        </div>
                    </div>


                </div>
            </div>
            <div class="item ">
                <div class="col-md-12">

                    <div class="col-md-3">
                        <div class="product-item">
                            <div class="image-box">
                                <img src="../assets/images/produkt1.png">
                            </div>
                            <div class="nazov-produktu">Nazov produktu na 2 rádky</div>
                            <div class="znacky">Outdoor</div>
                            <div class="cena-produktu">98 765 Kč</div>
                            <div class="pridatdokosika"><a target="_blank" target="_blank" href="#">Chci to koupit</a></div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="product-item">
                            <div class="image-box">
                                <img src="../assets/images/produkt1.png">
                            </div>
                            <div class="nazov-produktu">Nazov produktu na 2 rádky</div>
                            <div class="znacky">Outdoor</div>
                            <div class="cena-produktu">98 765 Kč</div>
                            <div class="pridatdokosika"><a target="_blank" target="_blank" href="#">Chci to koupit</a></div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="product-item">
                            <div class="image-box">
                                <img src="../assets/images/produkt1.png">
                            </div>
                            <div class="nazov-produktu">Nazov produktu na 2 rádky</div>
                            <div class="znacky">Outdoor</div>
                            <div class="cena-produktu">98 765 Kč</div>
                            <div class="pridatdokosika"><a target="_blank" target="_blank" href="#">Chci to koupit</a></div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="product-item">
                            <div class="image-box">
                                <img src="../assets/images/produkt1.png">
                            </div>
                            <div class="nazov-produktu">Nazov produktu na 2 rádky</div>
                            <div class="znacky">Outdoor</div>
                            <div class="cena-produktu">98 765 Kč</div>
                            <div class="pridatdokosika"><a target="_blank" target="_blank" href="#">Chci to koupit</a></div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
        <!-- Controls -->
        <a class="left carousel-control category-carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control category-carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <div class="col-md-12">
        <div class="col-md-4"></div>
        <div class="col-md-4 top-produkty-viac"><a target="_blank" href="#">Více z horolezectví</a></div>
        <div class="col-md-4"></div>
    </div>

<div class="col-md-12">
    <hr/>
<div class="nadpis-kategorie">
	<h3>Práce ve výškach</h3>
</div>
<?php include ('prace-ve-vyskach.php') ?>
<div class="row">
    <div class="col-md-12">
        <div class="col-md-4"></div>
        <div class="col-md-4 top-produkty-viac"><a target="_blank" href="#">Více z prace ve vyskach</a></div>
        <div class="col-md-4"></div>
    </div>
</div>
</div>
<div class="col-md-12">
    <hr/>
<div class="nadpis-kategorie">
	<h3>Camping</h3>
</div>
<?php include ('camping.php') ?>
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-4"></div>
            <div class="col-md-4 top-produkty-viac"><a target="_blank" href="#">Více z camping</a></div>
            <div class="col-md-4"></div>
        </div>
    </div>
</div>
<div class="col-md-12">
    <hr/>
<div class="nadpis-kategorie">
	<h3>Dětské vybavení</h3>
</div>
<?php include ('detske-vybaveni.php') ?>
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-4"></div>
            <div class="col-md-4 top-produkty-viac"><a target="_blank" href="#">Více z detske vybaveni</a></div>
            <div class="col-md-4"></div>
        </div>
    </div>
</div>

<div class="col-md-12">
    <hr/>
<div class="nadpis-kategorie">
	<h3>Pro zvířata</h3>
</div>
<?php include ('pro-zvirata.php') ?>
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-4"></div>
            <div class="col-md-4 top-produkty-viac"><a target="_blank" href="#">Více  pro zvirata</a></div>
            <div class="col-md-4"></div>
        </div>
    </div></div>
		</div>
	</div>
</div>
<script type="text/javascript">
    $(document).ready(function(){

        $('.ikonka-kategorie').mouseover(function(){

            var scroll_menu = $(".scroll-down-menu");

            if(scroll_menu.css('display') === 'none'){
                scroll_menu.fadeIn('fast').css('display','flex');
            }
        });
        $('.ikonka-kategorie').mouseout(function(){

            var scroll_menu = $(".scroll-down-menu");

            if(scroll_menu.css('display') === 'flex'){
                scroll_menu.fadeOut('fast').css('display','none');
            }
        })

        $('.ikonka-kategorie').click(function(){

            var click_menu = $('.click-menu');

            if( click_menu.css('display') == 'none'){

                click_menu.css('display','block');
                scroll_menu.fadeOut('fast').css('display','none');

                return 0;
            }

            click_menu.css('display','none');
        })
    })
</script>


<?php include('footer.php') ?>
</div>
