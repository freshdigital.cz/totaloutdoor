<?php include('header.php') ?>
    <style>
        .category-carousel-control span{
            padding:1%;
            background-color: #cccc00 !important;
        }
        .category-carousel-control span::before{
            color:white !important;
            text-shadow: none !important;
        }
        @media all and (max-width:984px){
            .carousel-control.left{
                left:-40px !important;
            }
            .carousel-control.right{
                right:-40px !important;
            }
        }
        @media all and (max-width:993px){
            .category-title h2::before{
                left: -4px !important;
            }
            .category-title h2::after{
                right: -4px !important;
            }
        }
        @media all and (max-width:401px){
            .ikonka-kategorie{
                width:100% !important;
            }
        }
        hr{
            border: 1px solid rgba(0,0,0,.15) !important;
        }
         .scroll-down-thing{
            display:none;
            -webkit-transition: display .3s ease-in;
            -moz-transition: display .3s ease-in;
            -ms-transition: display .3s ease-in;
            -o-transition: display .3s ease-in;
            transition: display .3s ease-in;
        }
        .filter-menu{
            -webkit-transition: display .3s ease-in;
            -moz-transition: display .3s ease-in;
            -ms-transition: display .3s ease-in;
            -o-transition: display .3s ease-in;
            transition: display .3s ease-in;
        }
.click-menu-text.click-menu-description a {
    color: #3e3e3e;
}
.click-menu-text.click-menu-description a:hover {
    color: #3e3e3e;
    text-decoration:none; 
}
    </style>
<div class="row">
	
	<div class="container">
        <div class="col-md-12">
            <div class="col-md-12" style="padding-left:5%;padding-right:5%;">
                <div class="col-md-3 col-sm-6 col-xs-12 ikonka-kategorie" style="margin-left: 0%;background-image: url('../assets/images/horolezectvi.png')">
                <span class="description-homepage">
                    horolezectví
                </span>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12 ikonka-kategorie" style="background-image:url('../assets/images/prace-ve-vyskach.png');background-size: cover;background-position: 80%;">
                <span class="description-homepage">
                    práce ve výškach
                </span>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12 ikonka-kategorie" style="background-image: url('../assets/images/camping.png');background-size: cover;
    background-position: 50% 75%;">
                <span class="description-homepage">
                    camping
                </span>
                </div>

                <div class="col-md-3 col-sm-6 col-xs-12 ikonka-kategorie" style="background-image: url('../assets/images/beautiful-2297215_1920.png');background-size: cover">
                <span class="description-homepage">
                    dětské vybavení
                </span>
                </div>

                <div class="col-md-3 col-sm-6 col-xs-12 ikonka-kategorie" style="background-image: url('../assets/images/dog-1557956_1920.jpg');background-size: cover;background-position: 100%;">
                <span class="description-homepage">
                    pro zvířata
                </span>
                </div>

            </div>


            <div class="banner-kategorie"><img src="../assets/images/banner-kategorie.png" />
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-6">
                    <div class="logo-kategorie"><img src="../assets/images/logo-kategorie.png" /></div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6">
                    <div class="text-banner"><p>Lorem ipsum dolor sit amet.</p></div>
                    <div class="tlacitko-banner2">Koupit teď</div>
                </div>
            </div>
        </div>
<div class="scroll-down-thing">
        <div class="scroll-down-menu">
            <div class="scroll-down-menu-item">
                <div class="row">
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <h4>Expressky karabiny</h4>
                    </div>
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <img class="scroll-down-image" src="../assets/images/produkt1.png"/>
                    </div>
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <p>
                            adfgadfhgafhadfjgadghadgshsdgfhasd
                        </p>
                    </div>
                </div>
            </div>
            <div class="scroll-down-menu-item">
                <div class="row">
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <h4>Expressky karabiny</h4>
                    </div>
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <img class="scroll-down-image" src="../assets/images/produkt1.png"/>
                    </div>
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <p>
                            adfgadfhgafhadfjgadghadgshsdgfhasd
                        </p>
                    </div>
                </div>
            </div>
            <div class="scroll-down-menu-item">
                <div class="row">
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <h4>Expressky karabiny</h4>
                    </div>
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <img class="scroll-down-image" src="../assets/images/produkt1.png"/>
                    </div>
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <p>
                            adfgadfhgafhadfjgadghadgshsdgfhasd
                        </p>
                    </div>
                </div>
            </div>
            <div class="scroll-down-menu-item">
                <div class="row">
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <h4>Expressky karabiny</h4>
                    </div>
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <img class="scroll-down-image" src="../assets/images/produkt1.png"/>
                    </div>
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <p>
                            adfgadfhgafhadfjgadghadgshsdgfhasd
                        </p>
                    </div>
                </div>
            </div>
            <div class="scroll-down-menu-item">
                <div class="row">
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <h4>Expressky karabiny</h4>
                    </div>
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <img class="scroll-down-image" src="../assets/images/produkt1.png"/>
                    </div>
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <p>
                            adfgadfhgafhadfjgadghadgshsdgfhasd
                        </p>
                    </div>
                </div>
            </div>
            <div class="scroll-down-menu-item">
                <div class="row">
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <h4>Expressky karabiny</h4>
                    </div>
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <img class="scroll-down-image" src="../assets/images/produkt1.png"/>
                    </div>
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <p>
                            adfgadfhgafhadfjgadghadgshsdgfhasd
                        </p>
                    </div>
                </div>
            </div>
            <div class="scroll-down-menu-item">
                <div class="row">
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <h4>Expressky karabiny</h4>
                    </div>
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <img class="scroll-down-image" src="../assets/images/produkt1.png"/>
                    </div>
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <p>
                            adfgadfhgafhadfjgadghadgshsdgfhasd
                        </p>
                    </div>
                </div>
            </div>
            <div class="scroll-down-menu-item">
                <div class="row">
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <h4>Expressky karabiny</h4>
                    </div>
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <img class="scroll-down-image" src="../assets/images/produkt1.png"/>
                    </div>
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <p>
                            adfgadfhgafhadfjgadghadgshsdgfhasd
                        </p>
                    </div>
                </div>
            </div>
            <div class="scroll-down-menu-item">
                <div class="row">
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <h4>Expressky karabiny</h4>
                    </div>
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <img class="scroll-down-image" src="../assets/images/produkt1.png"/>
                    </div>
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <p>
                            adfgadfhgafhadfjgadghadgshsdgfhasd
                        </p>
                    </div>
                </div>
            </div>


            </div>
            <div class="container" >
                <div class="row scroll-down-footer">

                    <div class="col-md-8 col-sm-8 col-xs-12 scroll-down-footer-item" style="padding-left:2% !important;border: 1px solid rgba(109, 187, 219, .9);">
                        <span>Značka: </span>
                        <img src="assets/images/brand1.png"/>
                        <img src="assets/images/brand2.png"/>
                        <img src="assets/images/brand3.png"/>
                        <img src="assets/images/brand4.png"/>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12 scroll-down-footer-item" >
                        <button class="btn btn-footer-scroll-down">
                            VÝPRODEJ
                        </button>
                    </div>

                </div>
            </div>
        </div>
		
		
    <div class="row">
        <div class="col-md-4 col-sm-12 col-xs-12">
            <br/>
            <nav class="breadcrumb">
            <a target="_blank" class="breadcrumb-item" href="index.php"><i class="fa fa-home"></i>&nbsp; katalóg produktu</a>
                > <a target="_blank" class="breadcrumb-item" href="category-2.php">  horolezectví </a>

            </nav>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">
            <div class="nadpis-kategorie hl-title">
                <h3>Horolezectví</h3>
            </div>
        </div>
    </div>
    <div class="click-menu">

        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">

                <div class="row">
                    <div class="col-md-4 col-sm-6 col-xs-12 click-menu-div">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 click-menu-subdiv">
                                <img src="../assets/images/produkt1.png" class="click-menu-image">
                                <h4 class="click-menu-text click-menu-description"><a href="../category-3.php">Káva a čaj</a></h4>
                            </div>

                        </div>
                    </div>

                    <div class="col-md-4 col-sm-6 col-xs-12 click-menu-div">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 click-menu-subdiv">
                                <img src="../assets/images/produkt1.png" class="click-menu-image">
                                <h4 class="click-menu-text click-menu-description"><a href="../category-3.php">Káva a čaj</a></h4>
                            </div>

                        </div>
                    </div>

                    <div class="col-md-4 col-sm-6 col-xs-12 click-menu-div">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 click-menu-subdiv">
                                <img src="../assets/images/produkt1.png" class="click-menu-image">
                                <h4 class="click-menu-text click-menu-description"><a href="../category-3.php">Káva a čaj</a></h4>
                            </div>

                        </div>
                    </div>

                    <div class="col-md-4 col-sm-6 col-xs-12 click-menu-div">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 click-menu-subdiv">
                                <img src="../assets/images/produkt1.png" class="click-menu-image">
                                <h4 class="click-menu-text click-menu-description"><a href="../category-3.php">Káva a čaj</a></h4>
                            </div>

                        </div>
                    </div>

                    <div class="col-md-4 col-sm-6 col-xs-12 click-menu-div">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 click-menu-subdiv">
                                <img src="../assets/images/produkt1.png" class="click-menu-image">
                                <h4 class="click-menu-text click-menu-description"><a href="../category-3.php">Káva a čaj</a></h4>
                            </div>

                        </div>
                    </div>

                    <div class="col-md-4 col-sm-6 col-xs-12 click-menu-div">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 click-menu-subdiv">
                                <img src="../assets/images/produkt1.png" class="click-menu-image">
                                <h4 class="click-menu-text click-menu-description"><a href="../category-3.php">Káva a čaj</a></h4>
                            </div>

                        </div>
                    </div>

                    <div class="col-md-4 col-sm-6 col-xs-12 click-menu-div">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 click-menu-subdiv">
                                <img src="../assets/images/produkt1.png" class="click-menu-image">
                                <h4 class="click-menu-text click-menu-description"><a href="../category-3.php">Kava a čaj</a></h4>
                            </div>

                        </div>
                    </div>

                    <div class="col-md-4 col-sm-6 col-xs-12 click-menu-div">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 click-menu-subdiv">
                                <img src="../assets/images/produkt1.png" class="click-menu-image">
                                <h4 class="click-menu-text click-menu-description"><a href="../category-3.php">Káva a čaj</a></h4>
                            </div>

                        </div>
                    </div>

                    <div class="col-md-4 col-sm-6 col-xs-12 click-menu-div">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 click-menu-subdiv">
                                <img src="../assets/images/produkt1.png" class="click-menu-image">
                                <h4 class="click-menu-text click-menu-description"><a href="../category-3.php">Káva a čaj</a></h4>
                            </div>

                        </div>
                    </div>



                </div>

            </div>

        </div>

    </div>
    <hr/>
    <div class="filter row">
        <div class="col-md-4 col-sm-6 col-xs-12">

            <span class="input-description">Řadit dle:</span>
            <select class="select-filter">
                <option>nejprodávanější</option>
            </select>

        </div>
        <div class="col-md-4 col-sm-6 col-xs-12 filter-options" >
            <div class="checkbox">
                <label><input type="checkbox" value="">Novinka</label>
            </div>

            <div class="checkbox">
                <label><input type="checkbox" value="">Top Produkt</label>
            </div>

            <div class="checkbox">
                <label><input type="checkbox" value="">Výprodej</label>
            </div>
        </div>

        <div class="col-md-4 col-sm-6 col-xs-12">
           <div class="pagination">
               <a target="_blank" href="#"> << </a>
               <a target="_blank" href="#"> < </a>
               <a target="_blank" href="#" class="active"> 1</a>
               <a target="_blank" href="#" > 2</a>
               <a target="_blank" href="#" > 3</a>
               <a target="_blank" href="#" > 4</a>
               <a target="_blank" href="#"> > </a>
               <a target="_blank" href="#"> >> </a>
           </div>
        </div>

    </div>

    <div class="row product-list">
        <div class="col-md-1">
        </div>
        <div class="col-md-12 col-ms-12 col-xs-12">
            <div class="row">

                <div class="col-md-3 col-sm-4 col-xs-12 ">
                    <div class="product-list-item">
                        <img src="../assets/images/produkt1.png" >

                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <span class="product-name">asdfasdfas</span>
                                <span class="brand">fdafds</span>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 col-sm-6"></div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <span class="previous-price">12345KČ</span>
                                <span class="new-price">5134KČ</span>
                            </div>
                        </div>

                        <button class="btn btn-buy">Chci to koupit</button>
                    </div>


                </div>

                <div class="col-md-3 col-sm-4 col-xs-12 ">
                    <div class="product-list-item">
                        <img src="../assets/images/produkt1.png" >

                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <span class="product-name">asdfasdfas</span>
                                <span class="brand">fdafds</span>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 col-sm-6"></div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <span class="previous-price">12345KČ</span>
                                <span class="new-price">5134KČ</span>
                            </div>
                        </div>

                        <button class="btn btn-buy">Chci to koupit</button>
                    </div>


                </div>

                <div class="col-md-3 col-sm-4 col-xs-12 ">
                    <div class="product-list-item">
                        <img src="../assets/images/produkt1.png" >

                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <span class="product-name">asdfasdfas</span>
                                <span class="brand">fdafds</span>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 col-sm-6"></div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <span class="previous-price">12345KČ</span>
                                <span class="new-price">5134KČ</span>
                            </div>
                        </div>

                        <button class="btn btn-buy">Chci to koupit</button>
                    </div>


                </div>

                <div class="col-md-3 col-sm-4 col-xs-12 ">
                    <div class="product-list-item">
                        <img src="../assets/images/produkt1.png" >

                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <span class="product-name">asdfasdfas</span>
                                <span class="brand">fdafds</span>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 col-sm-6"></div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <span class="previous-price">12345KČ</span>
                                <span class="new-price">5134KČ</span>
                            </div>
                        </div>

                        <button class="btn btn-buy">Chci to koupit</button>
                    </div>


                </div>

                <div class="col-md-3 col-sm-4 col-xs-12 ">
                    <div class="product-list-item">
                        <img src="../assets/images/produkt1.png" >

                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <span class="product-name">asdfasdfas</span>
                                <span class="brand">fdafds</span>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 col-sm-6"></div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <span class="previous-price">12345KČ</span>
                                <span class="new-price">5134KČ</span>
                            </div>
                        </div>

                        <button class="btn btn-buy">Chci to koupit</button>
                    </div>


                </div>

                <div class="col-md-3 col-sm-4 col-xs-12 ">
                    <div class="product-list-item">
                        <img src="../assets/images/produkt1.png" >

                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <span class="product-name">asdfasdfas</span>
                                <span class="brand">fdafds</span>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 col-sm-6"></div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <span class="previous-price">12345KČ</span>
                                <span class="new-price">5134KČ</span>
                            </div>
                        </div>

                        <button class="btn btn-buy">Chci to koupit</button>
                    </div>


                </div>

                <div class="col-md-3 col-sm-4 col-xs-12 ">
                    <div class="product-list-item">
                        <img src="../assets/images/produkt1.png" >

                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <span class="product-name">asdfasdfas</span>
                                <span class="brand">fdafds</span>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 col-sm-6"></div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <span class="previous-price">12345KČ</span>
                                <span class="new-price">5134KČ</span>
                            </div>
                        </div>

                        <button class="btn btn-buy">Chci to koupit</button>
                    </div>


                </div>

                <div class="col-md-3 col-sm-4 col-xs-12 ">
                    <div class="product-list-item">
                        <img src="../assets/images/produkt1.png" >

                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <span class="product-name">asdfasdfas</span>
                                <span class="brand">fdafds</span>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 col-sm-6"></div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <span class="previous-price">12345KČ</span>
                                <span class="new-price">5134KČ</span>
                            </div>
                        </div>

                        <button class="btn btn-buy">Chci to koupit</button>
                    </div>


                </div>

                <div class="col-md-3 col-sm-4 col-xs-12 ">
                    <div class="product-list-item">
                        <img src="../assets/images/produkt1.png" >

                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <span class="product-name">asdfasdfas</span>
                                <span class="brand">fdafds</span>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 col-sm-6"></div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <span class="previous-price">12345KČ</span>
                                <span class="new-price">5134KČ</span>
                            </div>
                        </div>

                        <button class="btn btn-buy">Chci to koupit</button>
                    </div>


                </div>
                <div class="col-md-3 col-sm-4 col-xs-12 ">
                    <div class="product-list-item">
                        <img src="../assets/images/produkt1.png" >

                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <span class="product-name">asdfasdfas</span>
                                <span class="brand">fdafds</span>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 col-sm-6"></div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <span class="previous-price">12345KČ</span>
                                <span class="new-price">5134KČ</span>
                            </div>
                        </div>

                        <button class="btn btn-buy">Chci to koupit</button>
                    </div>


                </div>
                <div class="col-md-3 col-sm-4 col-xs-12 ">
                    <div class="product-list-item">
                        <img src="../assets/images/produkt1.png" >

                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <span class="product-name">asdfasdfas</span>
                                <span class="brand">fdafds</span>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 col-sm-6"></div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <span class="previous-price">12345KČ</span>
                                <span class="new-price">5134KČ</span>
                            </div>
                        </div>

                        <button class="btn btn-buy">Chci to koupit</button>
                    </div>


                </div>
                <div class="col-md-3 col-sm-4 col-xs-12 ">
                    <div class="product-list-item">
                        <img src="../assets/images/produkt1.png" >

                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <span class="product-name">asdfasdfas</span>
                                <span class="brand">fdafds</span>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 col-sm-6"></div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <span class="previous-price">12345KČ</span>
                                <span class="new-price">5134KČ</span>
                            </div>
                        </div>

                        <button class="btn btn-buy">Chci to koupit</button>
                    </div>


                </div>
                <div class="col-md-3 col-sm-4 col-xs-12 ">
                    <div class="product-list-item">
                        <img src="../assets/images/produkt1.png" >

                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <span class="product-name">asdfasdfas</span>
                                <span class="brand">fdafds</span>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 col-sm-6"></div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <span class="previous-price">12345KČ</span>
                                <span class="new-price">5134KČ</span>
                            </div>
                        </div>

                        <button class="btn btn-buy">Chci to koupit</button>
                    </div>


                </div>
                <div class="col-md-3 col-sm-4 col-xs-12 ">
                    <div class="product-list-item">
                        <img src="../assets/images/produkt1.png" >

                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <span class="product-name">asdfasdfas</span>
                                <span class="brand">fdafds</span>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 col-sm-6"></div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <span class="previous-price">12345KČ</span>
                                <span class="new-price">5134KČ</span>
                            </div>
                        </div>

                        <button class="btn btn-buy">Chci to koupit</button>
                    </div>


                </div>
                <div class="col-md-3 col-sm-4 col-xs-12 ">
                    <div class="product-list-item">
                        <img src="../assets/images/produkt1.png" >

                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <span class="product-name">asdfasdfas</span>
                                <span class="brand">fdafds</span>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 col-sm-6"></div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <span class="previous-price">12345KČ</span>
                                <span class="new-price">5134KČ</span>
                            </div>
                        </div>

                        <button class="btn btn-buy">Chci to koupit</button>
                    </div>


                </div>
                <div class="col-md-3 col-sm-4 col-xs-12 ">
                    <div class="product-list-item">
                        <img src="../assets/images/produkt1.png" >

                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <span class="product-name">asdfasdfas</span>
                                <span class="brand">fdafds</span>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 col-sm-6"></div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <span class="previous-price">12345KČ</span>
                                <span class="new-price">5134KČ</span>
                            </div>
                        </div>

                        <button class="btn btn-buy">Chci to koupit</button>
                    </div>


                </div>


            </div>
        </div>
    </div>
    <br/>
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="row">
                    <div class="col-md-4 col-sm-4 col-xs-12"></div>
                    <div class="col-md-4 col-sm-4 col-xs-12 top-produkty-viac"><a target="_blank" href="#">Zobrazit dalších 16</a></div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="pagination bottom-pagination">
                            <a target="_blank" href="#"> << </a>
                            <a target="_blank" href="#"> < </a>
                            <a target="_blank" href="#" class="active"> 1</a>
                            <a target="_blank" href="#" > 2</a>
                            <a target="_blank" href="#" > 3</a>
                            <a target="_blank" href="#" > 4</a>
                            <a target="_blank" href="#"> > </a>
                            <a target="_blank" href="#"> >> </a>
                            <br/>
                            <a class="active">16</a> z 2000 položiek
                        </div>
                        <p>

                        </p>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function(){
		
		var hide;
		
        $('.ikonka-kategorie').on( "mouseenter", function() {
			clearTimeout(hide);
            //$('.filter-menu').css('display','none');
            $('.scroll-down-thing').css('display','block')
            $('.banner-kategorie').css('display','none')

        }).mouseleave(function(){
			
			
			 hide = setTimeout(function(){ 
				if( $('.scroll-down-thing:hover').length === 0 ){
					//$('.filter-menu').css('display','block');
					$('.scroll-down-thing').css('display','none')
					$('.banner-kategorie').css('display','block')
				}

				$('.scroll-down-thing').mouseleave(function(){
					//$('.filter-menu').css('display','block');
					$('.scroll-down-thing').css('display','none')
					$('.banner-kategorie').css('display','block')
				})
			 }, 300);  
        })

       /*  $('.ikonka-kategorie').mouseover(function(){

            var scroll_menu = $(".scroll-down-menu");

            if(scroll_menu.css('display') === 'none'){
                scroll_menu.fadeIn('fast').css('display','flex');
            }
        });
        $('.ikonka-kategorie').mouseout(function(){

            var scroll_menu = $(".scroll-down-menu");

            if(scroll_menu.css('display') === 'flex'){
                scroll_menu.fadeOut('fast').css('display','none');
            }
        }) */

        $('.ikonka-kategorie').click(function(){

            var click_menu = $('.click-menu');

            if( click_menu.css('display') == 'none'){

                click_menu.css('display','block');
                scroll_menu.fadeOut('fast').css('display','none');

                return 0;
            }

            click_menu.css('display','none');
        })
    })

</script>
    </div>

<?php include('footer.php') ?>