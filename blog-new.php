<div class="row">
	<div class="container blog-container">
		<div class="col-md-12">
			<style>
			@media (max-width: 450px){
				.read-more-blog{
					width:100%;
				}
			}
			</style>
			<div class="hl-title">
				<h2><span>Blog</span></h2>
			</div>
			<div class="row">
				<div class="col-md-6 col-sm-6 col-xs-12  blog-block">
					<div class="thumbnail-blog">
						<img src="../assets/images/blog/prvy.png"/>
					</div>
					<div class="blog-nazov">
						Lorem ipsum dolor it set
					</div>
					<div class="description-blog">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nemo enim ipsam voluptatem quia voluptas sit asper
						natur aut odit aut fugit, sed quia consequuntur magndolores eos qui ratione voluptatem sequi nesciunt.</div>

					<div class="read-more-blog"><a  href="blog_detail.php">Chci prečíst</a></div>
				</div>
				<div class="col-md-6 hidden-sm hidden-xs blog-block"><div class="thumbnail-blog">
						<img src="../assets/images/blog/druhy.png"/>
					</div>
					<div class="blog-nazov">
						Lorem ipsum dolor it set
					</div>
					<div class="description-blog">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nemo enim ipsam voluptatem quia voluptas sit asper
						natur aut odit aut fugit, sed quia consequuntur magndolores eos qui ratione voluptatem sequi nesciunt.</div>

					<div class="read-more-blog"><a href="blog_detail.php">Chci prečíst</a></div>
				</div>
				<div class="col-md-6 hidden-sm hidden-xs blog-block"><div class="thumbnail-blog">
						<img src="../assets/images/blog/prvy.png"/>
					</div>
					<div class="blog-nazov">
						Lorem ipsum dolor it set
					</div>
					<div class="description-blog">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nemo enim ipsam voluptatem quia voluptas sit asper
						natur aut odit aut fugit, sed quia consequuntur magndolores eos qui ratione voluptatem sequi nesciunt.</div>

					<div class="read-more-blog"><a  href="blog_detail.php">Chci prečíst</a></div>
				</div>
				<div class="col-md-6 hidden-sm hidden-xs blog-block"><div class="thumbnail-blog">
						<img src="../assets/images/blog/druhy.png"/>
					</div>
					<div class="blog-nazov">
						Lorem ipsum dolor it set
					</div>
					<div class="description-blog">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nemo enim ipsam voluptatem quia voluptas sit asper
						natur aut odit aut fugit, sed quia consequuntur magndolores eos qui ratione voluptatem sequi nesciunt.</div>

					<div class="read-more-blog"><a  href="blog_detail.php">Chci prečíst</a></div>
				</div>
			</div>

					
		</div>
        <br/>
		<div class="col-md-12">
			<div class="col-md-4 "></div>
			<div class="col-md-4 col-sm-12 col-xs-12"><div class="viac-tlacitko"><a target="_blank" href="#">VICE Z BLOGU</a></div></div>
				<div class="col-md-4"></div>
		</div>	
	</div>
</div>