<?php
    $active="blog";
    include('header.php');
?>
<div class="row">
	<div class="container">
		<div class="col-md-12">
			<div class="hl-title">
				<h2><span>Blog</span></h2>
			</div>
			<div class="checkbox-volba row">
				<div class="col-md-3 col-sm-3 col-xs-12 checkbox-blok1">
					<input type="checkbox" name="cestovani" value="cestovani"> <p class="blog-checkbox">Cestovaní</p>
				</div>
                <div class="col-md-3 col-sm-3 col-xs-12 checkbox-blok">
					<input type="checkbox" name="technologie" value="technologie"> <p class="blog-checkbox">Technológie</p>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-12 checkbox-blok">
					<input type="checkbox" name="nove-produkty" value="nove-produkty"> <p class="blog-checkbox">Nové produkty</p>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-12 checkbox-blok">
					<input type="checkbox" name="na-hory" value="na-hory"> <p class="blog-checkbox">Na hory</p>
				</div>
</div>
<div class="col-md-12">
    <div class="breadcrum"><a target="_blank" href="index.php"><i class="fa fa-home"></i></a> <a target="_blank" href="blog.php">blog</a> > <a target="_blank" href="blog.php">cestovateľské články</a> > <a target="_blank" href="blog.php">článek 1</a></div></div>
<div class="col-md-12">
	<div class="top-obrazok-blog"><img src="../assets/images/top-obrazok-blog.png" /></div>
	<div class="nadpis-blog-clanok">Lorem ipsum dolor sit amet.</div>
	<div class="datum-blog-clanok">10.10.2016</div>
	<div class="text1-blog-clanok">Stupňů plachetnice z má té sonda rybářský i ožije jejich uvést půdorysy, přišly pamětníkům, uvést ne uspořádaných studie psychologii obklady o dávný přetlakovaný gama britské opadají o pasivitou. Už slavení ze čističkami zaznamenal ta být oživováním snahy, tato hledá listopadovém ke ležet hibernujícím, s pod potřebuje ať přirovnávají. Závěry ně mor patří tj. čestná těla sezona obvyklým, vlastní mj. myslí? Ledničky mé pro zástupci tady a klonování softwarových činila vrátit monitorovaná operace, plynu práce hluboko že světěpodzemní žluté. Činu já či obcí přichází demenci obsahu s vede i radu budu. Čeští jeden ať byly 5300 m n.m.? Musel sloupy tepla solidních vědě kanady indy k nadšení drak domorodá úkony.</div>

</div>
<div class="col-md-12">
	<div class="col-md-6 col-sm-12 col-xs-12"><p class="text-v-clanku">Zda při moři lodích snu multi-dimenzionálním mění oborechjednou. Doma 110: vele, soudí optiku chaty můžeme objevení, se karavel nám i ze už ostře chladničce umístěním většině. Dlouhé, prohlídku zmizely duarte že evropa celý o subtropy hlavní. Nic tj. toho liška i polí z převážně kterém silou minerálů váš. Zemích je však u poměrně to přepravu myším vypráví, se větry archeolog mamuta brázdil svého sahají, síť ráno kontinent netopýři své opačně čeští. Ostrovu, jejíž mi zesílilo že změnily podíval, rozloučím závisí. Či počínaje tvrdě buků sama svou i nyní vládců sibiři, chtít malou jí uveřejněná snažit traektorii přírodu. Ostrovu, jejíž mi zesílilo že změnily podíval, rozloučím závisí. Či počínaje tvrdě buků sama svou i nyní vládců sibiři, chtít malou jí uveřejněná snažit traektorii přírodu. <br /><br />

Ostrovu, jejíž mi zesílilo že změnily podíval, rozloučím závisí. Či počínaje tvrdě buků sama svou i nyní vládců sibiři, chtít malou jí uveřejněná snažit traektorii přírodu.</p></div>
	<div class="col-md-6 col-sm-12 col-xs-12"><div class="obrazok-v-clanku">
            <img src="../assets/images/obrazok-v-clanku.png"/>
            <span class="text-article">
                Zda při moři lodích snu multi-dimenzionálním mění oborec
                jednou. Doma 110: vele, soudí optiku chaty můžeme objevení,
                se karavel nám i ze už ostře

            </span>
        </div>
    </div>
</div>
<div class="row">
	<div class="motto col-md-8 col-md-offset-2 col-sm-12 col-xs-12">„citace z článku, citace z článku, citace z článku, citace z článku,
    citace z článku,citace z článku,citace z článku,citace z článku,“</div>

</div>
<div class="col-md-12"><div class="obrazok2-v-clanku"><img src="../assets/images/obrazok2-v-clanku.png"/></div></div>
<div class="col-md-12">
	<div class="text-pod-mottom">Zda při moři lodích snu multi-dimenzionálním mění oborechjednou. Doma 110: vele, soudí optiku chaty můžeme objevení, se karavel nám i ze už ostře chladničce umístěním většině. Dlouhé, prohlídku zmizely duarte že evropa celý o subtropy hlavní. Nic tj. toho liška i polí z převážně kterém silou minerálů váš. Zemích je však u poměrně to přepravu myším vypráví, se větry archeolog mamuta brázdil svého sahají, síť ráno kontinent netopýři své opačně čeští. Ostrovu, jejíž mi zesílilo že změnily podíval, rozloučím závisí. Či počínaje tvrdě buků sama svou i nyní vládců sibiři, chtít malou jí uveřejněná snažit traektorii přírodu. Ostrovu, jejíž mi zesílilo že změnily podíval, rozloučím závisí. Či počínaje tvrdě buků sama svou i nyní vládců sibiři, chtít malou jí uveřejněná snažit traektorii přírodu.</div>
</div>
<div class="col-md-12">
	<div class="col-md-6 col-sm-12 col-xs-12"><div class="obrazok-v-clanku2">
            <img src="../assets/images/obrazok-v-clanku.png"/>
            <span class="text-article">
                Zda při moři lodích snu multi-dimenzionálním mění oborec
                jednou. Doma 110: vele, soudí optiku chaty můžeme objevení,
                se karavel nám i ze už ostře

            </span>
        </div></div>
	<div class="col-md-6 col-sm-12 col-xs-12"><p class="text-v-clanku2">Zda při moři lodích snu multi-dimenzionálním mění oborechjednou. Doma 110: vele, soudí optiku chaty můžeme objevení, se karavel nám i ze už ostře chladničce umístěním většině. Dlouhé, prohlídku zmizely duarte že evropa celý o subtropy hlavní. Nic tj. toho liška i polí z převážně kterém silou minerálů váš. Zemích je však u poměrně to přepravu myším vypráví, se větry archeolog mamuta brázdil svého sahají, síť ráno kontinent netopýři své opačně čeští. Ostrovu, jejíž mi zesílilo že změnily podíval, rozloučím závisí. Či počínaje tvrdě buků sama svou i nyní vládců sibiři, chtít malou jí uveřejněná snažit traektorii přírodu. </p>
		<div class="fb"><a target="_blank" href="https://www.facebook.com/TotalOutdoor.cz/"><i class="fa fa-facebook-official fa-2x" aria-hidden="true"></a></i>
</div>
<div class="autor-clanku">Autor článku:</div>
<div class="jmeno-prijimeni">Jméno prijímení:</div>
</div>
	

</div>

		</div>
	</div>

</div>



<div class="row">
	<div class="container blog-container">
		<div class="col-md-12">
			<div class="hl-title">
				<h2 class="blog-header"><span>DALŠÍ ČLÁNKY</span></h2></div>
				<div class="row">
                    <div class="col-md-6  blog-block">
                        <div class="thumbnail-blog">
                            <img src="../assets/images/blog/prvy.png"/>
                        </div>
                        <div class="blog-nazov">
                            Lorem ipsum dolor it set
                        </div>
                        <div class="description-blog">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nemo enim ipsam voluptatem quia voluptas sit asper
                            natur aut odit aut fugit, sed quia consequuntur magndolores eos qui ratione voluptatem sequi nesciunt.</div>

                        <div class="read-more-blog"><a href="blog_detail.php">Chci prečíst</a></div>
                    </div>
                    <div class="col-md-6 blog-block"><div class="thumbnail-blog">
                            <img src="../assets/images/blog/druhy.png"/>
                        </div>
                        <div class="blog-nazov">
                            Lorem ipsum dolor it set
                        </div>
                        <div class="description-blog">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nemo enim ipsam voluptatem quia voluptas sit asper
                            natur aut odit aut fugit, sed quia consequuntur magndolores eos qui ratione voluptatem sequi nesciunt.</div>

                        <div class="read-more-blog"><a  href="blog_detail.php">Chci prečíst</a></div></div>
                    <div class="col-md-6 blog-block"><div class="thumbnail-blog">
                            <img src="../assets/images/blog/prvy.png"/>
                        </div>
                        <div class="blog-nazov">
                            Lorem ipsum dolor it set
                        </div>
                        <div class="description-blog">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nemo enim ipsam voluptatem quia voluptas sit asper
                            natur aut odit aut fugit, sed quia consequuntur magndolores eos qui ratione voluptatem sequi nesciunt.</div>

                        <div class="read-more-blog"><a href="blog_detail.php">Chci prečíst</a></div></div>
                    <div class="col-md-6 blog-block"><div class="thumbnail-blog">
                            <img src="../assets/images/blog/druhy.png"/>
                        </div>
                        <div class="blog-nazov">
                            Lorem ipsum dolor it set
                        </div>
                        <div class="description-blog">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nemo enim ipsam voluptatem quia voluptas sit asper
                            natur aut odit aut fugit, sed quia consequuntur magndolores eos qui ratione voluptatem sequi nesciunt.</div>

                        <div class="read-more-blog"><a href="blog_detail.php">Chci prečíst</a></div>
                    </div>
                </div>

					
		</div>
		<div class="col-md-12">
			<div class="col-md-4"></div>
			<div class="col-md-4"><div class="viac-tlacitko"><a target="_blank" href="#">DALŠÍ ČLÁNKY NA BLOGU</a></div></div>
				<div class="col-md-4"></div>
		</div>	
	</div>
</div>
<?php include('footer.php') ?>